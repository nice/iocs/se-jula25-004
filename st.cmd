# ---
require essioc
require julabof25hl

# Set parameters when not using auto deployment
epicsEnvSet(IPADDR,     "172.30.32.15")
epicsEnvSet(IPPORT,     "4001")
epicsEnvSet(SYSTEM,     "SE-SEE")
epicsEnvSet(DEVICE,     "SE-JULA25-004")
epicsEnvSet(PREFIX,     "$(SYSTEM):$(DEVICE)")
epicsEnvSet(PORTNAME,   "$(PREFIX)")
epicsEnvSet(TEMPSCAN,   "1")
epicsEnvSet(CONFSCAN,   "10")
epicsEnvSet(LOCATION,   "$(SYSTEM); $(IPADDR)")
epicsEnvSet(IOCNAME,    "$(SYSTEM):$(DEVICE)")

iocshLoad("$(essioc_DIR)/common_config.iocsh")
iocshLoad("$(julabof25hl_DIR)julabof25hl.iocsh", "P=$(PREFIX), R=:, PORT=$(PORTNAME), ADDR=$(IPPORT), TEMPSCAN=$(TEMPSCAN), CONFSCAN=$(CONFSCAN)")
# ...
